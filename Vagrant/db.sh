#!/usr/bin/env bash

sudo apt-get update --fix-missing -y && sudo apt-get install -qq mysql-server

DB_HOST=10.153.185.100
DB_NAME=petclinic
DB_USER=petclinic
DB_PASS=A2mrRgM1sTaQSr3n
ROOT_PASS=A2mrRgM1sTaQSr3n

debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOT_PASS"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOT_PASS"

MYSQL=`which mysql`

Q1="CREATE DATABASE IF NOT EXISTS $DB_NAME;"
Q2="CREATE USER IF NOT EXISTS '$DB_USER'@'10.153.185.%' IDENTIFIED BY '$DB_PASS';"
Q3="GRANT ALL ON $DB_NAME.* TO '$DB_USER'@'10.153.185.%';"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

$MYSQL -uroot -p$ROOT_PASS -e "$SQL"

echo "Database $DB_NAME and user $DB_USER created with a password you chose"

sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

sudo service mysql restart

mysql -u "$DB_USER" -p"$DB_PASS" -h$DB_HOST -e "SHOW DATABASES"