#!/usr/bin/env bash

sudo apt-get update --fix-missing -y && sudo apt-get install openjdk-14-jdk -y

sudo useradd -s /bin/bash -d /home/elvin/ -m -G sudo elvin

sudo su - elvin

export MYSQL_PASS="A2mrRgM1sTaQSr3n"
export MYSQL_URL="jdbc:mysql://db:3306/petclinic"
export MYSQL_USER="petclinic"

if [[ -d "petclinic/" ]]; then
      cd petclinic
else
      sudo mkdir petclinic && cd petclinic/
      sudo git clone https://gitlab.com/elvin.huseynov/demo-1.git .
fi

sudo chmod +x mvnw

sudo ./mvnw clean package

sudo find /home/elvin/petclinic/target/ -name \*.jar -exec cp  {} /home/elvin/ \;

sudo touch /etc/systemd/system/petclinic.service
sudo cp /vagrant/petclinic.service /etc/systemd/system/petclinic.service 

sudo systemctl daemon-reload
sudo systemctl enable petclinic.service
sudo systemctl start petclinic

HEADERS=`curl -Is --connect-timeout ${1-5} http://app:8080/actuator/health`
CURLSTATUS=$?

# Check for timeout
if [ $CURLSTATUS -eq 28 ]
    then
        echo FALSE
else
    # Check HTTP status code
    HTTPSTATUS=`echo $HEADERS | grep HTTP | cut -d' ' -f2`
    if [ $HTTPSTATUS -le 399 ]
        then
            echo "Petclinic is Live!"
    else
        echo "Project is dead"
    fi
fi

